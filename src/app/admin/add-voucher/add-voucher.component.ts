import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-add-voucher',
  templateUrl: './add-voucher.component.html',
  styleUrls: ['./add-voucher.component.scss']
})
export class AddVoucherComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('user'))
  token = localStorage.getItem('token')
  imageUrl = null

  constructor() { }

  ngOnInit(): void {
    if (this.user.role != 'admin') {
      window.location.href = constant.WEB_URL + "dashboard";
    }
  }
  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const response = await Axios({
        method: 'post',
        url: `${constant.APP_URL}/api/user/upload`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${this.token}`
        }
      })
      //handle success
      this.imageUrl = response.data.imageUrl
    }
  }

  async onSubmit(form: NgForm) {
    const body = { ...form.value, image_url: this.imageUrl }

    let response = await Axios.post(`${constant.APP_URL}/api/voucher`, body, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    if (response.data.error) {
      alert(response.data.message)
    }
    else {
      alert("Berhasil buat voucher")
      window.location.href = constant.WEB_URL + "view-voucher";
    }
  }
}
