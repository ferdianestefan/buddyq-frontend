import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-edit-voucher',
  templateUrl: './edit-voucher.component.html',
  styleUrls: ['./edit-voucher.component.scss']
})
export class EditVoucherComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('user'))
  token = localStorage.getItem('token')
  voucherDetail = null
  imageUrl = null

  constructor(private activatedRoute: ActivatedRoute
  ) { }

  async getSingleVoucher() {
    const id = this.activatedRoute.snapshot.params.id;

    const response = await Axios.get(`${constant.APP_URL}/api/voucher/${id}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.voucherDetail = response.data
    this.imageUrl = response.data.image_url
    console.log(response.data);

  }

  ngOnInit(): void {
    if (this.user.role != 'admin') {
      window.location.href = constant.WEB_URL + "dashboard";
    }
    this.getSingleVoucher()
  }
  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const response = await Axios({
        method: 'post',
        url: `${constant.APP_URL}/api/user/upload`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${this.token}`
        }
      })
      //handle success
      this.imageUrl = response.data.imageUrl
    }
  }

  async onSubmit(form: NgForm) {
    const body = { ...form.value, image_url: this.imageUrl }

    let response = await Axios.put(`${constant.APP_URL}/api/voucher/${this.voucherDetail.id}`, body, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    if (response.data.error) {
      alert(response.data.message)
    }
    else {
      alert("Berhasil edit voucher")
      window.location.href = constant.WEB_URL + "view-voucher";
    }
  }
}
