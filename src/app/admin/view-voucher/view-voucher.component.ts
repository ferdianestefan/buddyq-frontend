import { Component, OnInit } from '@angular/core';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-view-voucher',
  templateUrl: './view-voucher.component.html',
  styleUrls: ['./view-voucher.component.scss']
})
export class ViewVoucherComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('user'))
  voucherData = []
  token = localStorage.getItem('token')
  selectedVoucher = null
  constructor() { }

  ngOnInit(): void {
    if (this.user.role != 'admin') {
      window.location.href = constant.WEB_URL + "dashboard";
    }
    this.getAllVoucher()
  }

  selectVoucher(item) {
    this.selectedVoucher = item
  }

  async deleteVoucher() {
    let response = await Axios.delete(`${constant.APP_URL}/api/voucher/${this.selectedVoucher.id}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    if (response) {
      alert("Berhasil delete")
      this.getAllVoucher()
    }
  }
  async getAllVoucher() {
    let response = await Axios.get(`${constant.APP_URL}/api/voucher`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.voucherData = response.data
  }
  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }
}
