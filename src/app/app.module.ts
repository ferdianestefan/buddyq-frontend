import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialDesign } from './material/material';

import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { MyFormComponent } from './user/my-form/my-form.component';
import { FormListComponent } from './user/my-form/form-list/form-list.component';
import { FaqComponent } from './faq/faq.component';
import { CoinComponent } from './coin/coin.component';
import { VoucherListComponent } from './coin/voucher-list/voucher-list.component';
import { VoucherDetailComponent } from './coin/voucher-detail/voucher-detail.component';
import { ExchangeHistoryComponent } from './coin/exchange-history/exchange-history.component';
import { HistoryDetailComponent } from './coin/history-detail/history-detail.component';
import { AdminComponent } from './admin/admin.component';
import { AddVoucherComponent } from './admin/add-voucher/add-voucher.component';
import { ViewVoucherComponent } from './admin/view-voucher/view-voucher.component';
import { EditVoucherComponent } from './admin/edit-voucher/edit-voucher.component';
import { ViewProfileComponent } from './user/view-profile/view-profile.component';
import { PreviewFormComponent } from './user/preview-form/preview-form.component';
import { CreateFormComponent } from './user/my-form/create-form/create-form.component';
import { SetRespondentComponent } from './user/set-respondent/set-respondent.component';
import { ViewFormComponent } from './user/view-form/view-form.component';



//Membuat Service
import { HttpClientModule } from '@angular/common/http';
import { FormDetailComponent } from './user/my-form/form-detail/form-detail.component';
import { FormResponseComponent } from './user/my-form/form-response/form-response.component';







@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserComponent,
    DashboardComponent,
    EditProfileComponent,
    MyFormComponent,
    FormListComponent,
    CreateFormComponent,
    FaqComponent,
    CoinComponent,
    VoucherListComponent,
    VoucherDetailComponent,
    ExchangeHistoryComponent,
    HistoryDetailComponent,
    AdminComponent,
    AddVoucherComponent,
    ViewVoucherComponent,
    EditVoucherComponent,
    ViewProfileComponent,
    PreviewFormComponent,
    SetRespondentComponent,
    ViewFormComponent,
    FormDetailComponent,
    FormResponseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialDesign,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
