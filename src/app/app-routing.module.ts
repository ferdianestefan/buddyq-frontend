import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { CreateFormComponent } from './user/my-form/create-form/create-form.component';
import { FormListComponent } from './user/my-form/form-list/form-list.component';
import { MyFormComponent } from './user/my-form/my-form.component';
import { FaqComponent } from './faq/faq.component';
import { VoucherListComponent } from './coin/voucher-list/voucher-list.component';
import { VoucherDetailComponent } from './coin/voucher-detail/voucher-detail.component';
import { ExchangeHistoryComponent } from './coin/exchange-history/exchange-history.component';
import { HistoryDetailComponent } from './coin/history-detail/history-detail.component';
import { AddVoucherComponent } from './admin/add-voucher/add-voucher.component';
import { ViewVoucherComponent } from './admin/view-voucher/view-voucher.component';
import { EditVoucherComponent } from './admin/edit-voucher/edit-voucher.component';
import { ViewProfileComponent } from './user/view-profile/view-profile.component';
import { PreviewFormComponent } from './user/preview-form/preview-form.component'
import { SetRespondentComponent } from './user/set-respondent/set-respondent.component';
import { ViewFormComponent } from './user/view-form/view-form.component'
import { FormDetailComponent } from './user/my-form/form-detail/form-detail.component';
import { FormResponseComponent } from './user/my-form/form-response/form-response.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'edit-profile',
    component: EditProfileComponent
  },
  {
    path: 'view-profile',
    component: ViewProfileComponent
  },
  {
    path: 'my-form',
    component: MyFormComponent
  },
  {
    path: 'form-list',
    component: FormListComponent
  },
  {
    path: 'create-form',
    component: CreateFormComponent
  },
  {
    path: 'preview-form',
    component: PreviewFormComponent
  },
  {
    path: 'set-respondent',
    component: SetRespondentComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'voucher-list',
    component: VoucherListComponent
  },
  {
    path: 'voucher-detail/:id',
    component: VoucherDetailComponent
  },
  {
    path: 'exchange-history',
    component: ExchangeHistoryComponent
  },
  {
    path: 'history-detail/:id',
    component: HistoryDetailComponent
  },
  {
    path: 'add-voucher',
    component: AddVoucherComponent
  },
  {
    path: 'view-voucher',
    component: ViewVoucherComponent
  },
  {
    path: 'edit-voucher/:id',
    component: EditVoucherComponent
  },
  {
    path: 'view-form/:id',
    component: ViewFormComponent
  },
  {
    path: 'form-detail/:id',
    component: FormDetailComponent
  },
  {
    path: 'form-response/:id',
    component: FormResponseComponent
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
