import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  token = localStorage.getItem('token')
  userProfile = null
  constructor(
    // private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    // let userId = this.activatedRoute.snapshot.params.id;
    this.getUserProfile()
  }

  async getUserProfile() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/profile`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    localStorage.setItem('user', JSON.stringify(response.data))

    this.userProfile = response.data
  }

  async onSubmit(form: NgForm) {
    const token = localStorage.getItem('token')

    let response = await Axios.put(`${constant.APP_URL}/api/user/` + this.userProfile.id, form.value, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (response.data.error) {
      alert(response.data.message)
    }
    else {
      window.location.href = constant.WEB_URL + "dashboard";
    }
  }
}
