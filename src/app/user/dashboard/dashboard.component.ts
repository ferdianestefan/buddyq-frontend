import { Component, OnInit } from '@angular/core';
import Axios from 'axios';
import { DashboardService } from './dashboard.service';
import constant from '../../constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  formData = []
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  walletBalance = 0

  constructor() { }


  ngOnInit() {
    this.getAllForm()
    this.getWallet()
    // if (!this.token) {
    //   window.location.href = constant.WEB_URL+"login";
    // }
  }

  async getAllForm() {
    let response = await Axios.get(`${constant.APP_URL}/api/form`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.formData = response.data
  }

  async getWallet() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/wallet`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.walletBalance = response.data.balance
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }
}
