import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import axios from "axios";
import constant from '../../constant';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(
    public http: HttpClient
  ) { }


  async getAllForm() {
    const url = "`${constant.APP_URL}l/api/form"
    const response = await axios.get(url)
    return response.data

  }
}
