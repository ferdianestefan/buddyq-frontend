import { Component, OnInit } from '@angular/core';
import Axios from 'axios';
import constant from '../../../constant';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.scss']
})
export class FormListComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('user'))
  token = localStorage.getItem('token')
  formData = []

  constructor() { }

  ngOnInit(): void {
    this.getAllForm()
  }

  async getAllForm() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/form`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.formData = response.data
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }
}
