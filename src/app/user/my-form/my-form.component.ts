import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.scss']
})
export class MyFormComponent implements OnInit {
  user = JSON.parse(localStorage.getItem('user'))

  constructor() { }

  ngOnInit(): void {
  }

}
