import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../../constant';

@Component({
  selector: 'app-form-detail',
  templateUrl: './form-detail.component.html',
  styleUrls: ['./form-detail.component.scss']
})
export class FormDetailComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  formDetail = null
  formId = null
  formLink = null

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.formId = this.activatedRoute.snapshot.params.id;
    this.formLink = constant.WEB_URL + 'view-form/' + this.formId
    this.getFormDetail()
  }

  generateArray(max) {
    var array = []
    for (let index = 1; index <= max; index++) {
      array.push(index)
    }
    return array
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getFormDetail() {
    let response = await Axios.get(`${constant.APP_URL}/api/form/${this.formId}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.formDetail = response.data

  }

  async deleteForm() {
    let response = await Axios.delete(`${constant.APP_URL}/api/form/${this.formId}/force`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    if (response) {
      window.location.href = constant.WEB_URL + "form-list";
    }
  }

  copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }
}
