import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../../constant';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  formInputs = []
  formDetail = null
  imageUrl = null
  isDesign = true
  isPreview = false
  isCriteria = false
  optionValue = ''
  scaleValue = []
  defaultColor = '#e3e4e6'
  templateId = null

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.template) {
        this.getFormDetail(params.template)
      }
    });
  }
  async getFormDetail(template) {
    let response = await Axios.get(`${constant.APP_URL}/api/form/${template}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.formInputs = response.data.form_inputs
    this.formInputs.forEach((val, index) => {
      if (val.input_type == 'scale') {

        this.scaleValue[index] = []
      }
    });
  }
  async fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const response = await Axios({
        method: 'post',
        url: `${constant.APP_URL}/api/user/upload`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${this.token}`
        }
      })
      //handle success
      this.imageUrl = response.data.imageUrl
    }
  }
  backToDesign() {
    this.isDesign = true

    this.isPreview = false
    this.isCriteria = false
  }
  backToPreview() {
    this.isPreview = true

    this.isCriteria = false
    this.isDesign = false
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  goToPreview(form: NgForm) {
    this.formDetail = { ...form.value, form_inputs: this.formInputs, image_url: this.imageUrl }
    console.log(this.formDetail);

    this.isPreview = true

    this.isDesign = false
    this.isCriteria = false
  }

  goToCriteria() {
    this.isCriteria = true

    this.isPreview = false
    this.isDesign = false
  }

  generateArray(max) {
    var array = []
    for (let index = 1; index <= max; index++) {
      array.push(index)
    }
    return array
  }

  async createForm(form: NgForm) {
    const apiBody = { ...this.formDetail, form_criterias: form.value }
    const response = await Axios.post(`${constant.APP_URL}/api/form`, apiBody, {
      headers: { Authorization: `Bearer ${this.token}` }
    })
  }


  addNewItem(type) {
    console.log(this.scaleValue);

    if (type == 'scale') {
      this.scaleValue[this.formInputs.length] = []
    }

    this.formInputs.push({
      input_type: type,
      question: "",
      key: Date.now(),
      required: true,
      option: [],
      default_value: null
    })
  }

  onRemoveInput(index) {
    this.formInputs.splice(index, 1)
  }

  onChangeQuestion(event, index) {
    let newData = {
      ...this.formInputs[index],
      question: event.target.value
    }
    this.formInputs.splice(index, 1, newData)

  }

  onChangeRequired(event, index) {
    let newData = {
      ...this.formInputs[index],
      required: event.checked
    }
    this.formInputs.splice(index, 1, newData)

  }

  // OPTION
  addOption(itemIndex) {
    this.formInputs[itemIndex].option.push(this.optionValue)
    this.optionValue = ''
  }
  removeOption(itemIndex, optionIndex) {
    this.formInputs[itemIndex].option.splice(optionIndex, 1)
  }

  // scale 
  applyScale(itemIndex) {
    this.formInputs[itemIndex].option = this.scaleValue[itemIndex]
    alert('Berhasil tersimpan')

  }
  copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }
}
