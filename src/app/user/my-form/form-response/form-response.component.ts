import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../../constant';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-form-response',
  templateUrl: './form-response.component.html',
  styleUrls: ['./form-response.component.scss']
})
export class FormResponseComponent implements OnInit {

  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  formDetail = null
  formResponses = []
  formId = null
  selectedForm = null
  downloadLink = null

  constructor(
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.formId = this.activatedRoute.snapshot.params.id;
    this.getFormDetail()
    this.getFormResponse()
  }

  isUrl(value) {
    console.log(value);

    if (value) {
      return value.includes("http://")
    }
    return false
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getFormResponse() {
    let response = await Axios.get(`${constant.APP_URL}/api/form/${this.formId}/response`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.formResponses = response.data
  }

  async getFormDetail() {
    let response = await Axios.get(`${constant.APP_URL}/api/form/${this.formId}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.formDetail = response.data
    this.downloadLink = this.sanitizer.bypassSecurityTrustUrl(constant.APP_URL + '/api/form/' + this.formDetail.id + '/response/download')
  }

  filterResponse(key) {
    let response = []

    for (const item of this.formResponses) {
      response.push(item[key])
    }

    return response
  }

  async deleteForm() {
    let response = await Axios.delete(`${constant.APP_URL}/api/form/${this.formId}/force`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    if (response) {
      window.location.href = constant.WEB_URL + "form-list";
    }
  }

  async onChangeSlide(event) {

    if (event.checked) {
      const response = await Axios.delete(`${constant.APP_URL}/api/form/${this.formId}`, {
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      })
      alert(response.data)
    }
    else {
      const response = await Axios.get(`${constant.APP_URL}/api/form/${this.formId}/restore`, {
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      })
      alert(response.data)

    }
  }

}
