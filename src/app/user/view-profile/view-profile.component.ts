import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {
  token = localStorage.getItem('token')
  user = null

  constructor() { }

  ngOnInit(): void {
    this.getUserProfile()
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getUserProfile() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/profile`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    localStorage.setItem('user', JSON.stringify(response.data))

    this.user = response.data
  }
  async onSubmit(form: NgForm) {

    const token = localStorage.getItem('token')
    const value = {
      ...form.value,
      gender: this.user.gender,
      name: this.user.name,
      birth_date: this.user.birth_date
    }
    let response = await Axios.put(`${constant.APP_URL}/api/user/` + this.user.id, value, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })

    if (response.data.error) {
      alert(response.data.message)
    }
    else {
      alert('Berhasil update data')
      window.location.href = constant.WEB_URL + "dashboard";
    }
  }
}


