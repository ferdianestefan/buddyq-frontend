import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-set-respondent',
  templateUrl: './set-respondent.component.html',
  styleUrls: ['./set-respondent.component.scss']
})
export class SetRespondentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

}
