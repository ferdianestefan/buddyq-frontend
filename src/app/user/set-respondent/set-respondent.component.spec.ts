import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetRespondentComponent } from './set-respondent.component';

describe('SetRespondentComponent', () => {
  let component: SetRespondentComponent;
  let fixture: ComponentFixture<SetRespondentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetRespondentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetRespondentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
