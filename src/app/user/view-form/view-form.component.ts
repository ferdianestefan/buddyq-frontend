import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../constant'
import _ from 'lodash'
@Component({
  selector: 'app-view-form',
  templateUrl: './view-form.component.html',
  styleUrls: ['./view-form.component.scss']
})
export class ViewFormComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  formDetail = null
  formId = null
  loading = false
  checkboxItems = {}
  files = []

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (!this.token) {
      window.location.href = constant.WEB_URL + "login";
    }

    this.formId = this.activatedRoute.snapshot.params.id;
    this.getFormDetail()
  }

  generateArray(max) {
    var array = []
    for (let index = 1; index <= max; index++) {
      array.push(index)
    }
    return array
  }
  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getFormDetail() {
    this.loading = true
    let response = await Axios.get(`${constant.APP_URL}/api/form/${this.formId}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.formDetail = response.data
    this.loading = false

  }

  onChangeCheckbox(key, opt) {
    if (this.checkboxItems[key] == undefined) {
      this.checkboxItems = {
        ...this.checkboxItems,
        [key]: [opt]
      }
    }
    else {
      if (this.checkboxItems[key].includes(opt)) {
        this.checkboxItems[key] = this.checkboxItems[key].filter(val => val != opt)
      }
      else {
        this.checkboxItems[key] = [...this.checkboxItems[key], opt]
      }
    }
  }

  async fileChange(event, key) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const response = await Axios({
        method: 'post',
        url: `${constant.APP_URL}/api/user/upload`,
        data: formData,
        headers: {
          'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${this.token}`
        }
      })
      //handle success
      this.files[key] = response.data.imageUrl
      console.log(this.files);

    }
  }
  async submitForm(form: NgForm) {
    if (form.status == 'INVALID') {
      alert('Mohon isi pertanyaan yang wajib diisi')
      return
    }
    const formVal = { ...form.value, ...this.checkboxItems, ...this.files }
    let filteredVal = _.pickBy(formVal, function (v, k) {
      return !k.includes('cb-');
    });

    const response = await Axios.post(
      `${constant.APP_URL}/api/form/${this.formId}/response`,
      { response_value: filteredVal },
      {
        headers: { Authorization: `Bearer ${this.token}` }
      })

    alert(response.data.message)
    window.location.href = constant.WEB_URL + "dashboard";

  }

  copyLink(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }
}
