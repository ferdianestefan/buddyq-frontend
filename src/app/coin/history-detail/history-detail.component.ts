import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss']
})
export class HistoryDetailComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  WalletBalance = 0
  historyDetail = null

  constructor(
    private activatedRoute: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this.getWallet()
    this.getDetail()
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getWallet() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/wallet`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.WalletBalance = response.data.balance
  }


  async getDetail() {
    const historyId = this.activatedRoute.snapshot.params.id;

    let response = await Axios.get(`${constant.APP_URL}/api/wallet/voucher-history/${historyId}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.historyDetail = response.data
  }


}
