import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-voucher-detail',
  templateUrl: './voucher-detail.component.html',
  styleUrls: ['./voucher-detail.component.scss']
})
export class VoucherDetailComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  WalletBalance = 0
  voucherDetail = null
  exchangeSuccess = false

  constructor(
    private activatedRoute: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this.getWallet()
    this.getDetail()
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getWallet() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/wallet`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.WalletBalance = response.data.balance
  }

  async getDetail() {
    const voucherId = this.activatedRoute.snapshot.params.id;

    let response = await Axios.get(`${constant.APP_URL}/api/voucher/${voucherId}`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.voucherDetail = response.data
  }

  async exchange() {
    this.exchangeSuccess = false
    let response = await Axios.get(`${constant.APP_URL}/api/voucher/${this.voucherDetail.id}/exchange`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.exchangeSuccess = response.data.success
    setTimeout(() => {
      location.reload()
    }, 3000);

  }

}
