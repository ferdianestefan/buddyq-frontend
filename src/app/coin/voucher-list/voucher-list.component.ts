import { Component, OnInit } from '@angular/core';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-voucher-list',
  templateUrl: './voucher-list.component.html',
  styleUrls: ['./voucher-list.component.scss']
})
export class VoucherListComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  WalletBalance = 0
  voucherData = []
  selectedVoucher = {
    id: null,
    code: ''
  }
  exchangeSuccess = false

  constructor() { }

  ngOnInit(): void {
    this.getAllVoucher()
    this.getWallet()
  }

  selectVoucher(item) {
    this.selectedVoucher = item
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getWallet() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/wallet`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.WalletBalance = response.data.balance
  }

  async getAllVoucher() {
    let response = await Axios.get(`${constant.APP_URL}/api/voucher`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.voucherData = response.data
  }

  async exchange() {
    this.exchangeSuccess = false
    let response = await Axios.get(`${constant.APP_URL}/api/voucher/${this.selectedVoucher.id}/exchange`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })

    this.exchangeSuccess = response.data.success
    setTimeout(() => {
      location.reload()
    }, 4000);
  }
}
