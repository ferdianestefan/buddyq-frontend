import { Component, OnInit } from '@angular/core';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-exchange-history',
  templateUrl: './exchange-history.component.html',
  styleUrls: ['./exchange-history.component.scss']
})
export class ExchangeHistoryComponent implements OnInit {
  token = localStorage.getItem('token')
  user = JSON.parse(localStorage.getItem('user'))
  WalletBalance = 0
  histories = []

  constructor() { }

  ngOnInit(): void {
    this.getWallet();
    this.getHistory()
  }

  logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    window.location.href = constant.WEB_URL + "login";
  }

  async getWallet() {
    let response = await Axios.get(`${constant.APP_URL}/api/user/wallet`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.WalletBalance = response.data.balance
  }

  async getHistory() {
    let response = await Axios.get(`${constant.APP_URL}/api/wallet/voucher-history`, {
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    this.histories = response.data
  }
}
