import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Axios from 'axios';
import { LoginService } from './login.service';
import constant from '../../constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  name: string
  token = localStorage.getItem('token')

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    this.name = this.loginService.onGet()
    if (this.token) {
      window.location.href = constant.WEB_URL + "dashboard";
    }
  }

  async onSubmit(form: NgForm) {
    const response = await Axios.post(`${constant.APP_URL}/api/user/login`, form.value)

    if (response.data.error) {
      alert(response.data.message)
    }
    else {
      const profile = await Axios.get(`${constant.APP_URL}/api/user/profile`, {
        headers: {
          Authorization: `Bearer ${response.data.token}`
        }
      })

      localStorage.setItem('token', response.data.token)
      localStorage.setItem('user', JSON.stringify(profile.data))

      if (profile.data.role == 'admin') {
        window.location.href = constant.WEB_URL + "view-voucher";
      }
      else {
        window.location.href = constant.WEB_URL + "dashboard";
      }

    }
  }
}
