import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Axios from 'axios';
import constant from '../../constant';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  token = localStorage.getItem('token')

  constructor() { }

  ngOnInit(): void {
    if (this.token) {
      window.location.href = constant.WEB_URL + "dashboard";
    }
  }

  async onSubmit(form: NgForm) {
    const responseRegister = await Axios.post(`${constant.APP_URL}/api/user/register`, form.value)
    alert(responseRegister.data.message)

    const responseLogin = await Axios.post(`${constant.APP_URL}/api/user/login`, {
      username: form.value.username,
      password: form.value.password,
    })

    if (responseLogin.data.error) {
      alert(responseLogin.data.message)
    }
    else {
      localStorage.setItem('token', responseLogin.data.token)
      window.location.href = constant.WEB_URL + "edit-profile";
    }
  }
}
